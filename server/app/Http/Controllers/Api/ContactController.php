<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ContactResource;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * Get all elements
     *
     * @return ContactResource
     */
    public function index(): ContactResource
    {
        return ContactResource::make(Contact::with('custom_fields')->get());
    }
}
