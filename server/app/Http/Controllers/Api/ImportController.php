<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\CsvData;
use App\Models\Contact;
use App\Http\Requests\ImportRequest;
use App\Http\Requests\CsvStoreRequest;
use App\Http\Resources\CsvDataResource;
use Illuminate\Http\JsonResponse;

/**
 * Class ImportController
 *
 * @package App\Http\Controllers\Api
 */
class ImportController extends Controller
{
    /**
     * Show
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $collumns = (new Contact())->getFillable();
        $collumns[] = 'No mapping';

        $data = [
            'columns' => $collumns,
            'csv_data' => CsvDataResource::make(CsvData::findOrFail($id))
        ];
        return response()->json($data);
    }

    /**
     * Store
     *
     * @param CsvStoreRequest $request
     * @return mixed
     */
    public function store(CsvStoreRequest $request)
    {
        $csv = getCsv($request->input('path'));

        if (count($csv) == 0) {
            return response()->noContent()->setStatusCode(204, 'CSV file is empty.');
        }

        return CsvData::insertGetId([
            'filename' => $request->input('filename'),
            'data' => json_encode($csv)
        ]);
    }

    /**
     * Import
     *
     * @param ImportRequest $request
     * @return string
     */
    public function import(ImportRequest $request)
    {
        $mappingResult = $request->input('mapping_result');

        $data = CsvData::select('data')->findOrFail($request->input('csv_id'));
        $csvData = json_decode($data->data);
        $csvDataHeader = array_shift($csvData);

        Contact::mapCsv($csvData, $csvDataHeader, $mappingResult);

        return 'succces import';
    }
}
