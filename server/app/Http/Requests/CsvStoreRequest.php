<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CsvStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'path' => $this->file('csv_file')->getRealPath(),
            'filename' => $this->file('csv_file')->getClientOriginalName()
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv_file' => 'required|file'
        ];
    }
}
