<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DataLogger
{
    private $startTime;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->startTime = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if (env('DATALOGGER', true)) {
            $endTime = microtime(true);
            $filename = 'logger_data_' . date('d-m-y') . '.log';
            $dataToLog = 'Time: ' . gmdate("F j, Y, g:i a") . "\n";
            $dataToLog .= 'Duration: ' . number_format($endTime - LARAVEL_START, 3) . "\n";
            $dataToLog .= 'IP Address: ' . $request->ip() . "\n";
            $dataToLog .= 'URL: ' . $request->fullUrl() . "\n";
            $dataToLog .= 'Method: ' . $request->method() . "\n";
            $dataToLog .= 'Input: ' . $request->getContent() . "\n";
            // $dataToLog .= 'Output: ' . $response->getContent() . "\n";
            $filepath = storage_path('datalogger'); //this fails
            if(!File::exists($filepath)){
                File::makeDirectory($filepath, 0755, true, true);
            };
            File::append(storage_path('datalogger' . DIRECTORY_SEPARATOR . $filename), $dataToLog . "\n" . str_repeat("=", 20) . "\n\n");
        }
    }
}
