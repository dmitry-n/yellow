<?php

if (!function_exists('rememberKey')) {

    /**
     * Recursive ksort function
     *
     * @param string $path
     * @return array
     */
    function getCsv(string $path): array
    {
        $csv = [];
        $lines = file($path, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $key => $value) {
            $csv[$key] = str_getcsv($value);
        }

        return $csv;
    }
}
