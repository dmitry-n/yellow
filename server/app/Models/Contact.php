<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contact extends Model
{
    public $fillable = [
        'team_id',
        'unsubscribed',
        'first_name',
        'last_name',
        'phone',
        'email',
        'sticky_phone_number_id',
        'twitter_id',
        'fb_messenger_id',
        'time_zone'
    ];

    /**
     * @return HasMany
     */
    public function custom_fields(): HasMany
    {
        return $this->hasMany(CustomFields::class, 'contact_id', 'id');
    }

    /**
     * TODO: fixed (O) great
     *
     * @param $csvData
     * @param $csvDataHeader
     * @param $mappingResult
     */
    public static function mapCsv($csvData, $csvDataHeader, $mappingResult)
    {
        foreach ($csvData as $row) {
            $custom_buffer = [];
            $contact = new Contact();
            foreach ($row as $index => $data_elem) {
                $header_elem = $csvDataHeader[$index];

                $field = null;
                for ($i = 0; $i < count($mappingResult); $i++) {
                    if ($mappingResult[$i][1] == $header_elem) {
                        if ($mappingResult[$i][2] !== 'No mapping') {
                            $field = $mappingResult[$i][2];
                        } else {
                            $custom_buffer[] = [
                                'key' => $mappingResult[$i][1],
                                'value' => $data_elem
                            ];
                        }
                    }
                }

                if ($field) {
                    $contact->$field = $data_elem;
                }
            }
            $contact->save();
            $contact->custom_fields()->createMany($custom_buffer);
        }
    }
}
