<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomFields extends Model
{
    protected $table = 'custom_fields';

    public $fillable = [
        'key',
        'value'
    ];
}
