<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->bigIncrements('id');
            $table->integer('team_id')->unsigned();
            $table->char('unsubscribed_status', 191)->collation('utf8mb4_unicode_ci')->default('none');
            $table->char('first_name', 191)->collation('utf8mb4_unicode_ci')->nullable()->index();
            $table->char('last_name', 191)->collation('utf8mb4_unicode_ci')->nullable()->index();
            $table->char('phone', 191)->collation('utf8mb4_unicode_ci')->index();
            $table->char('email', 191)->collation('utf8mb4_unicode_ci')->nullable()->index();
            $table->integer('sticky_phone_number_id')->collation('utf8mb4_unicode_ci')->nullable()->index();
            $table->char('twitter_id')->collation('utf8mb4_unicode_ci')->nullable()->index();
            $table->integer('fb_messenger_id')->collation('utf8mb4_unicode_ci')->nullable();
            $table->char('time_zone')->collation('utf8mb4_unicode_ci')->nullable();
            
            $table->timestamps();
        });

        DB::statement("ALTER TABLE contacts AUTO_INCREMENT = 256;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
