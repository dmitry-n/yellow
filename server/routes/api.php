<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::apiResource('csv', 'ImportController')->only('store', 'show');
    Route::apiResource('contacts', 'ContactController')->only('index');

    Route::post('import', 'ImportController@import');
});
