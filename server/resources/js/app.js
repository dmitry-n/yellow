import Vue from 'vue'

import store from './store';

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import App from './views/App'
import Upload from './components/Upload'
import Mapping from './components/Mapping'
import ViewCSV from './components/ViewCSV'

window.Vue = require('vue');
Vue.config.productionTip = false;

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'upload',
            component: Upload
        },
        {
            path: '/mapping',
            name: 'mapping',
            component: Mapping,
        },
        {
            path: '/viewcsv',
            name: 'viewcsv',
            component: ViewCSV,
        },
    ],
});

window.Vue = new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    render: h => h(App),
});
