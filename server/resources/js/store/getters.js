const getters = {
  upload_csv_file: state => state.app.upload_csv_file,
  upload_csv_file_header: state => state.app.upload_csv_file_header,

  id_csv_data: state => state.app.id_csv_data,
  mapping_data: state => state.app.mapping_data,
  mapping_result: state => state.app.mapping_result,

  response_errors: state => state.app.response_errors,

  contacts_load: state => state.app.contacts_load
};

export default getters;
