import axios from 'axios';

const state = {
    upload_csv_file: null,
    upload_csv_file_header: true,

    id_csv_data: -1,
    mapping_data: null,
    mapping_result: null,

    response_errors: '',

    contacts_load: null
};

const mutations = {
    UPLOAD_CSV_FILE: (state, file) => {
        state.upload_csv_file = file;
    },

    ID_CSV_DATA: (state, id) => {
        state.id_csv_data = id;
    },
    MAPPING_DATA: (state, data) => {
        state.mapping_data = data;
    },
    MAPPING_RESULT: (state, data) => {
        state.mapping_result = data;
    },

    RESPONSE_ERRORS: (state, errors) => {
        state.response_errors = errors;
    },

    CONTACTS_LOAD: (state, data) => {
        state.contacts_load = data;
    },
};

const actions = {
    uploadCSVFile({commit}, {file}) {
        const fileData = new FormData();
        fileData.append('csv_file', file);

        axios.post('api/csv', fileData, {headers: {'Content-Type': 'multipart/form-data'}}).then(function (response) {
            commit('UPLOAD_CSV_FILE', file);
            let id_csv = response.data;
            commit('ID_CSV_DATA', id_csv);

            axios.get('api/csv/' + id_csv).then(function (response) {
                commit('MAPPING_DATA', response.data);
                window.Vue.$router.push('mapping');
            }).catch(function (response) {
                commit('RESPONSE_ERRORS', response.response.data.message);
            });
        }).catch(function (response) {
            commit('RESPONSE_ERRORS', response.response.data.message);
        });
    },

    idCsvData({commit}, {id}) {
        commit('ID_CSV_DATA', id);
    },
    mappingData({commit}, {data}) {
        commit('MAPPING_DATA', data);
    },
    mappingResult({commit}, {data}) {
        commit('MAPPING_RESULT', data);

        const fileData = new FormData();
        fileData.append('csv_id', state.id_csv_data);
        fileData.append('mapping_result', JSON.stringify(state.mapping_result));

        axios.post('api/import', fileData).then(function (response) {
            commit('ID_CSV_DATA', -1);
            commit('MAPPING_DATA', null);
            commit('MAPPING_RESULT', null);
            window.Vue.$router.push('viewcsv');
        }).catch(function (response) {
            commit('RESPONSE_ERRORS', response.response.data.message);
        });
    },

    responseErrors({commit}, {errors}) {
        commit('RESPONSE_ERRORS', errors);
    },

    contactsLoad({commit}) {
        axios.get('api/contacts').then(function (response) {
            commit('CONTACTS_LOAD', response.data.data);
        }).catch(function (response) {
            commit('RESPONSE_ERRORS', response.response.data.message);
        });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};
