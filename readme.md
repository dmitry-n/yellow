## Installation

Development environment requirements:
- [Docker](https://www.docker.com) >= 17.06
- [Docker Compose](https://docs.docker.com/compose/install/)


## Before starting - dev stage
```bash
$ cd server
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ cp .env.dev .env
$ docker-compose run --rm --no-deps yellow-server composer install
$ docker-compose run --rm --no-deps yellow-server php artisan key:generate
$ docker-compose run --rm --no-deps yellow-server php artisan migrate --seed
$ docker-compose up -d
```

## Before starting - prod stage
```bash
$ cd server
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ $ cp .env.prod .env
$ docker-compose run --rm --no-deps yellow-server composer install --optimize-autoloader --no-dev
$ docker-compose run --rm --no-deps yellow-server php artisan key:generate
$ docker-compose run --rm --no-deps yellow-server php artisan migrate --seed
$ docker-compose run --rm --no-deps yellow-server composer cache-all
$ docker-compose up -d
```
Now you can access the application via [http://localhost:85](http://localhost:85).

## Useful commands
Seeding the database:
```bash
$ docker-compose run --rm yellow-server php artisan db:seed
```

Running tests:
```bash
$ ./vendor/bin/phpunit --cache-result --order-by=defects --stop-on-defect
```

In development environnement, rebuild the database:
```bash
$ docker-compose run --rm yellow-server php artisan migrate:fresh --seed
```